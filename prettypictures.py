from gasp import *

begin_graphics()

def draw_person(x,y):
    Arc((x,y),80,140,260) ##Head
    Circle((x+25,y+25),10) ##Eyes
    Circle((x-25,y+25),10)
    Line((x,y), (x-20,y-20)) ##Nose
    Line((x-20,y-20), (x,y-30))
    Arc((x,y),60,225,90) ##Mouth
    Arc((x+25,y+25),20,30,120)
    Arc((x-25,y+25),20,30,120)

    Line((x,y-80), (x,y-230))##Body
    Line((x,y-230),(x-30,y-330))##Legs
    Line((x,y-230),(x+30,y-330))
    Line((x,y-125),(x-50,y-145))##Arms
    Line((x,y-125),(x+50,y-145))

    Arc((x,y+50),65,0,180)##Hat
    Line((x-65,y+50), (x+100,y+50))




draw_person(300,360)

update_when('key_pressed')

end_graphics()
