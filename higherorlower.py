from random import randint

def main():
    number = randint(1,1000)
    print("OK, I've thought of a number between 1 and 1000.\n")

    guess_count = 0
    while True:

        guess = int(input("Make a guess. "))
        guess_count += 1

        if guess == number:
            print("That was my number. Well done!\n")
            break

        if guess < number:
            print("That's too low.\n")

        if guess > number:
            print("That's too high.\n")

    print("You took " + str(guess_count) + " guesses.")
    gamecheck = input("Would you like another game? ")
    if gamecheck == "yes":
        main()
    else:
        print("OK. Bye!")


main()
