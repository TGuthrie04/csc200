import sys
import spotipy
import spotipy.util as util
from spotipy.oauth2 import SpotifyOAuth
from spotipy.oauth2 import SpotifyClientCredentials
import json
import requests
import random

class automated:

    def topplaylsitmaker():
        scope = "playlist-modify-public, playlist-modify-private, user-library-read, user-top-read"
        username = "tdog854"

        token = SpotifyOAuth(scope=scope,username=username)
        spotifyObject = spotipy.Spotify(auth_manager=token)

        playlist_name = input("Enter a playlist name:")
        playlist_description = input("Enter a playlist description:")

        spotifyObject.user_playlist_create(user=username,name=playlist_name,public=True,description=playlist_description)

        user_input = input("How many of your most played songs do you want in your playlist?\n")
        x = 0
        while x == 0:
            user_input2 = input("What timeframe for your top songs do you want? 1 month, 6 months, all time?\n")
            if user_input2 == "1 month":
                user_input2 =  "short_term"
                x=1
            if user_input2 == "6 months":
                user_input2 = "medium_term"
                x=1
            if user_input2 == "all time":
                user_input2 = "long_term"
                x=1
            else:
                x=0


        songliste = []


        result = spotifyObject.current_user_top_tracks(limit=user_input, offset=0, time_range=user_input2)

        for item in result["items"]:
            songliste.append(item["uri"])

        prePlaylist = spotifyObject.user_playlists(user=username)
        playlist = prePlaylist["items"][0]["id"]

        spotifyObject.playlist_add_items(playlist_id=playlist,items=songliste)


    def recentplayedmix():
        scope = "playlist-modify-public, playlist-modify-private, user-library-read, user-top-read, user-read-recently-played, user-read-playback-position"
        username = "tdog854"

        token = SpotifyOAuth(scope=scope,username=username)
        spotifyObject = spotipy.Spotify(auth_manager=token)

        playlist_name = input("Enter a playlist name:")
        playlist_description = input("Enter a playlist description:")

        spotifyObject.user_playlist_create(user=username,name=playlist_name,public=True,description=playlist_description)

        user_input = input("How many of your recently played songs do you want in your playlist?\n")



        songliste = []


        result = spotifyObject.current_user_recently_played(limit=user_input)



        for item in result["items"]:
            songliste.append(item["track"]["uri"])

        prePlaylist = spotifyObject.user_playlists(user=username)
        playlist = prePlaylist["items"][0]["id"]

        spotifyObject.playlist_add_items(playlist_id=playlist,items=songliste)

    def expiermentalplaylist():
        scope = "playlist-modify-public, playlist-modify-private, user-library-read, user-top-read, user-read-recently-played, user-read-playback-position"
        username = "tdog854"

        token = SpotifyOAuth(scope=scope,username=username)
        spotifyObject = spotipy.Spotify(auth_manager=token)

        playlist_name = input("Enter a playlist name:")
        playlist_description = input("Enter a playlist description:")

        spotifyObject.user_playlist_create(user=username,name=playlist_name,public=True,description=playlist_description)

        nameliste = []
        playliste = []

        result = spotifyObject.user_playlists(user=username, limit=50, offset=0)
        for item in result["items"]:
            nameliste.append(item["name"])
            playliste.append(item["id"])

        for number, letter in enumerate(nameliste[1:]):
            print(number + 1, letter)

        choose_playlist = input("Choose a playlist to base your recommendations off of\n")

        tracks = spotifyObject.playlist_tracks(playlist_id=playliste[int(choose_playlist)], limit=100)

        songliste = []

        for item in tracks["items"]:
            songliste.append(item["track"]["uri"])


        reccomendation = spotifyObject.recommendations(seed_tracks=songliste[0:4], limit=30)

        songliste2 = []

        for item in reccomendation["tracks"]:
            songliste2.append(item["uri"])

        prePlaylist = spotifyObject.user_playlists(user=username)
        playlist = prePlaylist["items"][0]["id"]

        spotifyObject.playlist_add_items(playlist_id=playlist,items=songliste2)

    def expiermentaltrack():
        scope = "playlist-modify-public, playlist-modify-private, user-library-read, user-top-read, user-read-recently-played, user-read-playback-position"
        username = "tdog854"

        token = SpotifyOAuth(scope=scope,username=username)
        spotifyObject = spotipy.Spotify(auth_manager=token)

        playlist_name = input("Enter a playlist name:")
        playlist_description = input("Enter a playlist description:")

        spotifyObject.user_playlist_create(user=username,name=playlist_name,public=True,description=playlist_description)


        list_of_songs = []
        x = 0
        while x != 5:
            user_input = input('Enter 5 songs for the base of the playlist:')
            result = spotifyObject.search(q=user_input)
            print("1: " + (result['tracks']['items'][0]['name']) + " - " + (result['tracks']['items'][0]['artists'][0]['name']))
            print("2: " + (result['tracks']['items'][1]['name']) + " - " + (result['tracks']['items'][1]['artists'][0]['name']))
            print("3: " + (result['tracks']['items'][2]['name']) + " - " + (result['tracks']['items'][2]['artists'][0]['name']))
            print("4: " + (result['tracks']['items'][3]['name']) + " - " + (result['tracks']['items'][3]['artists'][0]['name']))
            print("5: " + (result['tracks']['items'][4]['name']) + " - " + (result['tracks']['items'][4]['artists'][0]['name']))
            choice_input = input('Choose a song:')
            if choice_input == "quit":
                continue
            else:
                list_of_songs.append(result['tracks']['items'][(int(choice_input)-1)]['uri'])

            x += 1

        reccomendation = spotifyObject.recommendations(seed_tracks=list_of_songs[0:4], limit=30)

        songliste2 = []

        for item in reccomendation["tracks"]:
            songliste2.append(item["uri"])

        prePlaylist = spotifyObject.user_playlists(user=username)
        playlist = prePlaylist["items"][0]["id"]

        spotifyObject.playlist_add_items(playlist_id=playlist,items=songliste2)
