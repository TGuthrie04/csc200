import random

c = 0
while c != 10:
    x = random.randint(0,12)
    y = random.randint(0,12)
    print("What's " + str(x) + " times " + str(y) +"?")
    a = input(">")
    if str(a) == str(x*y):
        print("That’s right – well done.")
    else:
        print("No, I’m afraid the answer is " + str(x*y))
    c += 1

print("finished")
