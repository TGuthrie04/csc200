import json
import os

os.system('clear')
 
with open('data.json') as json_file:
    data = json.load(json_file)
    for p in data['guests']:
        print('Name: ' + p['name'])
        print('Age: ' + p['age'])
        for item in p['allergies']:
            print(item)
        print('')
