# Notes

*these are notes as most of my work so far has been research instead of constant commits, so I am compiling my notes into one document to show what I have so far*


## Spotify API

To access the Spotify API I first had to create a Spotify developer account on the spotify website
Doing this gave me a 2 Client ID's that my code could use to identify where to make the playlists (i.e. what account)

## Virtual Environment

Since I am creating an app, I created an virtual environment using the python module virtual env

## Spotipy

do `pip install spotipy` to install the module needed to interface with the spotify API
First in the virtual env we need to set our variables as they need to be defined, yet putting them in a document is a security risk as the tokens shouldn't be easily visible to someone who downloads the app.
To do this on windows you just type   `set SPOTIPY_CLIENT_ID = xxxxxxxxxxxxxxx`

Now that our variables are defined, we can right the program that will create a playlist. The playlists contents will be in a json format so make sure to import json.
Running the code in your virtual environment will create  playlist with the name and description inputted

##Notes Pt 2

Today was another day of notes as I wasn't prepared to take on the next challenge yet. I spent most of today going over the spotify documentation to see how i could automate a playlist. I think I have a plan.

Using the `playlist_tracks` command I can see all the tracks that a playlist has in it. So first I will ask the user to pick a playlist they want to base this automated playlist on then I will look at the artists of the tracks and add different songs by them, as well as keeping some songs from the original in.

Not to much to write down today, but I definitley got a way better understanding of the spotify API as a whole.

## source spotifyApp/bin/activate Because I always forget the activation path
