import xml.etree.cElementTree as ET
import os

##parsing data
root = ET.parse('xmlex.xml')
data = root.getroot()
lst = data.findall('contacts/contact')

##printing data
os.system('clear')
print('You currently have ', len(lst), 'contact(s) on your phone')

for item in lst:
    print('\nName:', item.find('name').text)
    print('Phone Number:', item.find('phonenumber').text)
    print('Date of Birth:', item.find('birthday').text)
