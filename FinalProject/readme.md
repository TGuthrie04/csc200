Obligatory read me file

Plans for this project
I want to be able to create a spotify playlist using the spotify API through python.
To do this I will research the spotify API by looking at its documentation

I also plan to add some sort of feature that creates playlists based of something, noting too special though. Like maybe you could type an artist and it would generate a playlist of similar songs? I will have to see what the API has to offer for that

The details of the program are a little up in the air but the overall goal is to have a program that creates spotify playlists
