import spotipy
from spotipy.oauth2 import SpotifyOAuth
import json

def playlistmake():
    scope = "playlist-modify-public"
    username = "tdog854"

    token = SpotifyOAuth(scope=scope,username=username)
    spotifyObject = spotipy.Spotify(auth_manager = token)


    playlist_name = input("Enter a playlist name:")
    playlist_description = input("Enter a playlist description:")

    spotifyObject.user_playlist_create(user=username,name=playlist_name,public=True,description=playlist_description)


    list_of_songs = []

    while True:
        user_input = input('Enter songs for the playlist:')
        if user_input == "quit":
            break
        else:
            result = spotifyObject.search(q=user_input)
            print("1: " + (result['tracks']['items'][0]['name']) + " - " + (result['tracks']['items'][0]['artists'][0]['name']))
            print("2: " + (result['tracks']['items'][1]['name']) + " - " + (result['tracks']['items'][1]['artists'][0]['name']))
            print("3: " + (result['tracks']['items'][2]['name']) + " - " + (result['tracks']['items'][2]['artists'][0]['name']))
            print("4: " + (result['tracks']['items'][3]['name']) + " - " + (result['tracks']['items'][3]['artists'][0]['name']))
            print("5: " + (result['tracks']['items'][4]['name']) + " - " + (result['tracks']['items'][4]['artists'][0]['name']))
            choice_input = input('Choose a song:')
            if choice_input == "skip":
                continue
            if choice_input == "quit":
                break
            else:
                list_of_songs.append(result['tracks']['items'][(int(choice_input)-1)]['uri'])




    prePlaylist = spotifyObject.user_playlists(user=username)
    playlist = prePlaylist['items'][0]['id']


    spotifyObject.user_playlist_add_tracks(user=username,playlist_id=playlist,tracks=list_of_songs)
